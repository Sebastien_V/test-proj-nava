import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MeteorsSearchComponent } from './meteors-search/meteors-search.component';

const routes: Routes = [
  {
    path: '',
    component: MeteorsSearchComponent,
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MeteorsRoutingModule { }
