import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MeteorsSearchComponent } from './meteors-search.component';

describe('MeteorsSearchComponent', () => {
  let component: MeteorsSearchComponent;
  let fixture: ComponentFixture<MeteorsSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MeteorsSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MeteorsSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
