import { Component, OnInit } from '@angular/core';
import { MeteorsSearchService } from '../services/meteors-search.service';

@Component({
  selector: 'app-meteors-search',
  templateUrl: './meteors-search.component.html',
  styleUrls: ['./meteors-search.component.css']
})

export class MeteorsSearchComponent implements OnInit {

  public allMeteors: any[] = [];
  public years: any[] = [];
  public meteorsOfSelectedYear: any[]
  public selectedYear: number = null;
  public selectedMass: string = null;

  public foundedMeteors: any[] = [];

  public meteorsLoading: boolean = true;
  public isNotificationShown: boolean = false;
  public notificationMessage = '';

  constructor(private meteorsSearchService: MeteorsSearchService) { }

  ngOnInit() {
    this.getAllMeteors();
  }

  getAllMeteors() {
    this.meteorsSearchService.getAllMeteors().subscribe(
      (data: []) => {
        data.sort((a: any, b: any) => {
          return new Date(a.year).getTime() - new Date(b.year).getTime();
        });
        this.allMeteors = data;
        this.meteorsLoading = false;
        this.generateYears()
      },
      err => {
        this.meteorsLoading = false;
      }
    )
  }

  dateSorter(x, y) {
    var a: any = new Date(x);
    var b: any = new Date(y);
    return a - b;
  }

  generateYears() {
    let currentYear = new Date().getFullYear();
    do {
      this.years.push(currentYear);
      currentYear--;
    } while (currentYear >= 0);
  }

  onYearInputChange() {
    this.filterMeteorsByDate();
    this.selectedMass = null;
    if (this.foundedMeteors.length === 0) {
      this.showNotification('In the selected year meteorites did not fall!');
    }
  }

  filterMeteorsByDate() {
    this.meteorsOfSelectedYear = this.allMeteors.filter((meteor) => {
      return new Date(meteor.year).getFullYear() === this.selectedYear
    });
    this.foundedMeteors = JSON.parse(JSON.stringify(this.meteorsOfSelectedYear))
  }

  onMassInputChange() {
    if (isNaN(Number(this.selectedMass))) { return }
    if (this.selectedMass && Number(this.selectedMass) > 0) {
      this.filterMeteorsByMass();
    }
  }

  filterMeteorsByMass() {
    this.foundedMeteors = this.meteorsOfSelectedYear.filter((meteor) => {
      return Number(meteor.mass) >= Number(this.selectedMass)
    });
    if (this.foundedMeteors.length === 0) {
      this.showNotification('Mass was not found, jumping to first-year where there is a mass that fits the criteria!');
      this.showSpecificYear();
    }
  }

  showSpecificYear() {
    for (const meteor of this.allMeteors) {
      if (meteor.id === 5064) debugger;
      if (meteor.mass >= this.selectedMass) {
        debugger;
        this.selectedYear = new Date(meteor.year).getFullYear();
        this.filterMeteorsByDate();
        break;
      }
    }
    this.showNotification('In the selected year meteorites did not fall!')
  }

  showNotification(msg: string) {
    this.isNotificationShown = true;
    this.notificationMessage = msg;
    setTimeout(() => {
      this.isNotificationShown = false;
      this.notificationMessage = '';
    }, 10000);
  }

}
