import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { SharedModule } from '../shared/shared.module';

import { MeteorsSearchComponent } from './meteors-search/meteors-search.component';
import { MeteorsRoutingModule } from './meteors-roouting.module';
import { MeteorsSearchService } from './services/meteors-search.service';

@NgModule({
  declarations: [MeteorsSearchComponent],
  imports: [
    CommonModule,
    SharedModule,
    MeteorsRoutingModule,
    FormsModule
  ],
  providers: [
    MeteorsSearchService
  ]
})
export class MeteorsSearchModule { }
