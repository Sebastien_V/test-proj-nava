import { Injectable } from '@angular/core';
import { HttpService } from 'src/app/shared/services/http.service';

@Injectable({
  providedIn: 'root'
})
export class MeteorsSearchService {

  constructor(private httpService: HttpService) { }

  public meteorsUrl = 'https://data.nasa.gov/resource/y77d-th95.json';

  getAllMeteors() {
    return this.httpService.get(this.meteorsUrl);
  }
}
